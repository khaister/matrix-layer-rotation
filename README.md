# Matrix Layer Rotation

I've originally found this problem on [HackerRank](https://www.hackerrank.com/challenges/matrix-rotation-algo). I share the solution that I've come up here.

## Description

You are tasked with rotating a $$m \times n$$ matrix $$r$$ times counter-clockwise. See the link above for a more detailed/complete description.

## Analysis/Observation

* The top-left corner of every loop always have the same column and row indices, _e.g._ $$a_{00}$$, $$a_{11}$$, etc.
* One of the two dimensions is always an even number. This correlates with the number of loops in the matrix. For example, a $$4 \times 5$$ matrix has two loops that can rotate.
* It would be fast if the rotation can be done in-place, without having to reconstruct the matrix/array.

## Solution

1. Identify the number of loops, $$l$$, in the matrix: $$l = \text{min}(m, n) / 2$$.

2. For each of the loops, rotate. (See the revised solution below.)

### Rotation

Since the rotation is done in place, at least one value would be overwritten at any time during the rotation. Therefore, we must temporarily store this value and reassign it after one rotation. Essentially, we create a "gap" in the loop. For simplicity, we can store the top-left value of the loop.

1. Store the value of the top-left value.
2. Going from left to right on the top row, for each value (except the last one), assign to itself the value from the column to its right.
3. Going from top to bottom on the right column, for each value (except the last one), assign to itself the value from the row right beneath it.
4. Going from right to left on the bottom row, for each value (except the last one), assign to itself the value from the column to its left.
5. Going from bottom to top on the left column, for each value (except the last two), assign to itself the value from the row right above it.
6. Lastly, assign the top-left value that we stored in step 1 to the row right underneath it.

We have completed one rotation. We just need to repeat the steps to complete $$r$$ rotations.

## Performance Consideration

The program works and provides expected output. However, when the matrix size or rotation number is very large, the program takes longer than expected to run. There are several things we can do to improve performance.

1. Each loop can rotate independent of one another. We can leverage TPL (or any other appropriate parallel computation technique) to perform the rotation in parallel.
2. We don't have to move the values through the intermediate coordinates. We just need to know the final coordinate after the complete rotation, where we can just move the value there.
3. We can find out what the **net** movement is since moving $$r$$ times is the same as moving $$r \text{ mod } d$$, where $$d$$ is the dimension of the loop.

Considering only options #2 and #3, #2 seems better since we don't have to waste time doing multiple rotation; we just need to move the values one time each. But the logic for calculating the final coordinates seems complex so I'm opted for option 3. Option #1 can also be used in combination but this can be saved for future exploration/experiment/study.

As a result, a revised solution is as follows.

### Revised Solution

1. Identify the number of loops, $$l$$, in the matrix: $$l = \text{min}(m, n) / 2$$.

2. For each of the loops, rotate a net movement $$r_{\text{net}} = r \text{ mod } d$$, where $$d = 2 \times (m + n) - 4$$ since we don't want to count the corners twice.

## Implementation

Look at the [repository](https://gitlab.com/khaister/matrix-layer-rotation) and see the `RotateMatrix()` method on _src/Program.cs_ for the implementation details.

### Build & Run

```sh
git clone https://gitlab.com/khaister/matrix-layer-rotation
cd matrix-layer-rotation/src

# run
dotnet run

# or run with fed-in input
dotnet run < input.txt
```

The content of _input.txt_ is how you enter the data at the command line for the program.

```txt
4 4 2
1 2 3 4
5 6 7 8
9 10 11 12
13 14 15 16
```