﻿using System;

namespace MatrixRotation
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] mnr = Console.ReadLine().Split(' ');

            int m = Convert.ToInt32(mnr[0]);

            int n = Convert.ToInt32(mnr[1]);

            int r = Convert.ToInt32(mnr[2]);

            int[][] matrix = new int[m][];

            for (int i = 0; i < m; i++)
            {
                matrix[i] = Array.ConvertAll(Console.ReadLine().Split(' '), matrixTemp => Convert.ToInt32(matrixTemp));
            }

            RotateMatrix(matrix, m, n, r);

            for (var rowIndex = 0; rowIndex < m; rowIndex++)
            {
                for (var columnIndex = 0; columnIndex < n; columnIndex++)
                {
                    Console.Write(matrix[rowIndex][columnIndex] + " ");
                }

                Console.Write(Environment.NewLine);
            }            
        }

        static void RotateMatrix(int[][] matrix, int m, int n, int r)
        {
            var numberOfLoops = Math.Min(m, n) / 2;

            var currentLoopHeight = m;
            var currentLoopWidth = n;

            for (var loopIndex = 0; loopIndex < numberOfLoops; loopIndex++)
            {
                var currentLoopDimension = 2 * (currentLoopHeight + currentLoopWidth) - 4; //substract 4 to avoid counting the corner twice
                var netR = r % currentLoopDimension;

                for (var rotation = 0; rotation < netR; rotation++)
                {
                    var topLeftCoordinate = new Coordinate() { RowIndex = loopIndex, ColumnIndex = loopIndex };
                    var topRightCoordinate = new Coordinate() { RowIndex = topLeftCoordinate.RowIndex, ColumnIndex = topLeftCoordinate.ColumnIndex + currentLoopWidth - 1 };
                    var bottomLeftCoordinate = new Coordinate() { RowIndex = topLeftCoordinate.RowIndex + currentLoopHeight - 1, ColumnIndex = topLeftCoordinate.ColumnIndex };
                    var bottomRightCoordinate = new Coordinate() { RowIndex = bottomLeftCoordinate.RowIndex, ColumnIndex = topRightCoordinate.ColumnIndex };

                    var topLeftValue = matrix[topLeftCoordinate.RowIndex][topLeftCoordinate.ColumnIndex];

                    // work on the TOP edge
                    for (var columnIndex = topLeftCoordinate.ColumnIndex; columnIndex < topRightCoordinate.ColumnIndex; columnIndex++)
                    {
                        matrix[topLeftCoordinate.RowIndex][columnIndex] = matrix[topLeftCoordinate.RowIndex][columnIndex + 1];
                    }

                    // work on the RIGHT edge
                    for (var rowIndex = topRightCoordinate.RowIndex; rowIndex < bottomRightCoordinate.RowIndex; rowIndex++)
                    {
                        matrix[rowIndex][topRightCoordinate.ColumnIndex] = matrix[rowIndex + 1][topRightCoordinate.ColumnIndex];
                    }

                    // work on the BOTTOM edge
                    for (var columnIndex = bottomRightCoordinate.ColumnIndex; columnIndex > bottomLeftCoordinate.ColumnIndex; columnIndex--)
                    {
                        matrix[bottomRightCoordinate.RowIndex][columnIndex] = matrix[bottomRightCoordinate.RowIndex][columnIndex - 1];
                    }

                    // work on the LEFT edge
                    for (var rowIndex = bottomLeftCoordinate.RowIndex; rowIndex > topLeftCoordinate.RowIndex + 1; rowIndex--)
                    {
                        matrix[rowIndex][bottomLeftCoordinate.ColumnIndex] = matrix[rowIndex - 1][bottomLeftCoordinate.ColumnIndex];
                    }

                    // move the final piece in the loop
                    matrix[topLeftCoordinate.RowIndex + 1][topLeftCoordinate.ColumnIndex] = topLeftValue;                    
                }

                currentLoopHeight -= 2;
                currentLoopWidth -= 2;
            }
        }

        private class Coordinate
        {
            public int ColumnIndex { get; set; }
            public int RowIndex { get; set; }
        }
    }
}
